extends Node2D

var scene_fading_to

func set_player_spritesheet(charno, spritesheet):
	get_node("Players").get_child(charno).texture = spritesheet;
func set_enemy_spritesheet(charno, spritesheet):
	get_node("Enemies").get_child(charno).texture = spritesheet;
	
func set_character_name(charno, char_name):
	get_node("Battle Map/UI/Character Info").get_child(charno).text = char_name;
func set_character_hp(charno, current, maximum):
	get_node("Battle Map/UI/Character Info/Stats/HP").get_child(charno).text = String(current) + "/" + String(maximum);
func set_character_sp(charno, current, maximum):
	get_node("Battle Map/UI/Character Info/Stats/SP").get_child(charno).text = String(current) + "/" + String(maximum);
func set_character_attacks(charno, attack_names):
	get_node("Battle Map/UI/Character Info").set_character_attacks(charno, attack_names);

func set_enemy_name(charno, char_name):
	get_node("Battle Map/UI/Enemy Info").get_child(charno).text = char_name;
func set_enemy_hp(charno, current, maximum):
	get_node("Battle Map/UI/Enemy Info/HP").get_child(charno).text = String(current) + "/" + String(maximum);
	
func do_animation(animation_name):
	get_node("AnimationPlayer").play(animation_name);
func set_status_text(text):
	get_node("Battle Map/UI/Status Text").text = text;

# Called when animation is done.
func _on_finished_animation(anim_name):
	get_node("..").animation_finished();
	
func get_selected_character():
	return get_node("Battle Map/UI/Character Info").currently_selected_button;
func get_selected_attack():
	return get_node("Battle Map/UI/Attacks").currently_selected_button;
func get_selected_enemy():
	return get_node("Battle Map/UI/Enemy Info").currently_selected_button;
func enemy_selected():
	get_node("..").player_selected_enemy();
	
func hide_all_enemies():
	for child in get_node("Enemies").get_children():
		child.visible = false;
func show_tarrasque():
	get_node("Tarrasque").visible = true;

func set_character_dead(charno):
	get_node("Players").get_child(charno).modulate = Color("#b00b13");
func set_enemy_dead(charno):
	get_node("Enemies").get_child(charno).modulate = Color("#b00b13");
	
func fade_to_scene(scene_name):
	pass;
	
