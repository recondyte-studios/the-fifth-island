extends Container

var currently_selected_button = 0;
var is_focused = true;
var skip_next_tick = false;
var char_attacks = [["","",""],["","",""],["","",""],["","",""]];

func _process(delta):
	if skip_next_tick:
		skip_next_tick = false;
		return;
	if is_focused:
		update_ui_input();
		update_attacks();
		update_ui_selection();

func set_focus():
	skip_next_tick = true;
	is_focused = true;
func unfocus():
	is_focused = false;
	
func ui_unselect_all():
	# set all the buttons to their default color:
	for child in range(4):
		set_font_color(get_child(child), "#000000");
	
func update_ui_selection():
	ui_unselect_all();
	set_font_color(get_child(currently_selected_button), "#b00b13");
	
func set_font_color(button, color):
	button.add_color_override("font_color", Color(color));

func update_ui_input():
	if Input.is_action_just_pressed('ui_up'):
		select_next_child();
	elif Input.is_action_just_pressed('ui_down'):
		select_prev_child();
	elif Input.is_action_just_pressed('ui_select'):
		do_selection();
		return;

func select_prev_child():
	currently_selected_button += 1;
	if currently_selected_button >= get_children().size() - 1:
		currently_selected_button = 0;
func select_next_child():
	currently_selected_button -= 1;
	if currently_selected_button < 0:
		currently_selected_button = get_children().size() - 2;
		
func do_selection():
	get_node("../Attacks").set_focus();
	unfocus();

func set_character_attacks(charno, attacks):
	char_attacks[charno] = attacks;

func update_attacks():
	for attack in range(3):
		get_node("../Attacks").set_attack_name(attack, char_attacks[currently_selected_button][attack]);

