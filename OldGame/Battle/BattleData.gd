extends Resource
# This array contains resource paths to the character sheets of enemies
export(Array) var enemies;
enum BattleType {
	BOSS_BATTLE,
	BASIC_BATTLE,
}
export(BattleType) var battle_type;
