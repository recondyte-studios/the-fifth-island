extends Node

# TODO:
# This is only exported so we can make new battle datas
# ================================================
var battle_data;
var enemies = Array();
var players = Array();
var is_player_turn = false;
var selected_battler = 0;

class InitiativeSorter:
	static func sort(a,b):
		return a.get_initiative() > b.get_initiative();

func _ready():
	begin_battle(PlayerData.battle_name);

func _process(delta):
	ui_update_status();

func begin_battle(battle_type):
	if battle_type == "Secret":
		set_up_secret_battle();
		return;
	# Reset the seed:
	randomize();
	battle_data = load("res://Battle/Assets/BattleTypes/" + battle_type + ".tres");
	var player_initiative = set_up_players();
	var enemy_initiative = set_up_enemies();
	
	# Get the person who is going first
	if player_initiative >= enemy_initiative:
		is_player_turn = true;
	else:
		is_player_turn = false;
	# And now we update the UI of the battle
	set_ui();
	# And we start their turn
	start_turn();
	
func set_up_secret_battle():
	print("Special battle");
	get_node("BattleScene").hide_all_enemies();
	get_node("BattleScene").show_tarrasque();
	set_up_players();
	get_node("BattleScene").do_animation("Rocks Fall");
	set_status_text("Rocks fall. Everybody dies");

func set_up_players():
	var initiative = 0;
	# Add the party to our battlers
	for player in PlayerData.get_children():
		player.roll_initiative();
		initiative += player.get_initiative();
		players.append(player);
	return initiative;
func set_up_enemies():
	var initiative = 0;
	for enemy in battle_data.enemies:
		print(enemy);
		var ch_sheet = load("res://Battle/Assets/Enemies/" + enemy + ".tres");
		var character_data = preload("res://Support Scenes/Character.gd").new();
		character_data.character_sheet = ch_sheet;
		character_data.full_restore();
		character_data.roll_initiative();
		initiative += character_data.get_initiative();
		enemies.append(character_data);
	return initiative;

func start_turn():
	ui_start_turn();
	if !is_player_turn:
		do_enemy_turn();

	
func do_enemy_turn():
	print("Doing enemy turn");
	selected_battler = choose_random_enemy();
	if enemies[selected_battler].get_is_dead():
		do_enemy_turn();
		return;
	prints("selected battler:", selected_battler);
	var random_spell_number = choose_random_spell(current_battler());
	prints("random spell:", random_spell_number);
	var random_target = players[choose_random_player()];
	prints("random player:", random_target.get_name());
	attack_target(random_target, random_spell_number);
	
func choose_random_enemy():
	return randi() % enemies.size();
func choose_random_spell(battler):
	return randi() % battler.get_no_spells();
func choose_random_player():
	return randi() % players.size();
	
func end_turn():
	is_player_turn = !is_player_turn;
	if have_defeated():
		ui_fade_to_overworld();
	if have_been_defeated():
		ui_fade_to_game_over();
	start_turn();

func have_defeated():
	# Check to see if all enemies are dead, and false if not.
	for enemy in enemies:
		if !enemy.get_is_dead():
			return false;
	return true;

func have_been_defeated():
	for player in players:
		if !player.get_is_dead():
			return false;
	return true;
	
func current_battler():
	if is_player_turn:
		return players[selected_battler];
	else:
		return enemies[selected_battler];
	
func get_moves(playerno):
	if is_player_turn:
		return players[playerno].get_spell_names();
	else:
		return enemies[playerno].get_spell_names();

func attack_target(target, spellno):
	match current_battler().do_spell(target, spellno):
		-1:
			ui_move_fail();
		-2:
			ui_no_sp();
		var move_damage:
			ui_move_succeed(spellno, move_damage);

func finish_attack():
	end_turn();

# /------------\
# |UI Functions|
# \------------/

func ui_update_death_status():
	for enemy in range(enemies.size()):
		if enemies[enemy].get_is_dead():
			get_node("BattleScene").set_enemy_dead(enemy);
	for player in range(players.size()):
		if players[player].get_is_dead():
			get_node("BattleScene").set_character_dead(player);

func player_selected_enemy():
	var battle_ui = get_node("BattleScene");
	selected_battler = battle_ui.get_selected_character();
	var target_id = battle_ui.get_selected_enemy();
	var spellno = battle_ui.get_selected_attack();
	if enemies[target_id].get_is_dead():
		set_status_text("Target is dead");
		return;
	attack_target(enemies[target_id], spellno);

func set_ui():
	set_players_ui();
	set_enemy_ui();
	ui_update_player_status();

func set_players_ui():
	var battle_ui = get_node("BattleScene");
	for player in range(players.size()):
		battle_ui.set_character_name(player, players[player].get_name());
		battle_ui.set_player_spritesheet(player, players[player].get_spritesheet());
		battle_ui.set_character_attacks(player, players[player].get_spell_names());
func set_enemy_ui():
	var battle_ui = get_node("BattleScene");
	for enemy in range(enemies.size()):
		battle_ui.set_enemy_name(enemy, enemies[enemy].get_name());
		battle_ui.set_enemy_spritesheet(enemy,enemies[enemy].get_spritesheet());
		
		
# Do cool UI stuff for when a turn is started
func ui_start_turn():
	set_turn_status_text();
	ui_update_status();
func set_turn_status_text():
	set_status_text(get_turn_text() + " turn!");
func set_status_text(text):
	get_node("BattleScene").set_status_text(text);
func get_turn_text():
	if is_player_turn:
		return "Player";
	else:
		return "Enemy";
	
func ui_move_fail():
	set_status_text("Move failed!");
	finish_attack();
func ui_no_sp():
	set_status_text("Not enough sp");
	finish_attack();
func ui_move_succeed(spellno, damage):
	set_status_text("Spell power: " + String(damage));
	do_spell_animation(spellno);

func do_spell_animation(spellno):
	var spell_animation_name = current_battler().get_spell_animation(spellno);
	if spell_animation_name == "EnemyMelee":
		spell_animation_name += String(selected_battler + 1);
	get_node("BattleScene").do_animation(spell_animation_name);
func animation_finished():
	finish_attack();
	

func ui_update_status():
	ui_update_player_status();
	ui_update_enemy_status();
	ui_update_death_status();
func ui_update_player_status():
	update_player_hp();
	update_player_sp();
func update_player_hp():
	for player in range(players.size()):
		get_node("BattleScene").set_character_hp(player, players[player].get_hp(), players[player].get_max_hp());
func update_player_sp():
	for player in range(players.size()):
		get_node("BattleScene").set_character_sp(player, players[player].get_sp(), players[player].get_max_sp());
func ui_update_enemy_status():
	update_enemy_hp();
func update_enemy_hp():
	for enemy in range(enemies.size()):
		get_node("BattleScene").set_enemy_hp(enemy, enemies[enemy].get_hp(), enemies[enemy].get_max_hp());
func ui_fade_to_overworld():
	get_tree().change_scene("res://Overworld/Overworld.tscn");
func ui_fade_to_game_over():
	get_tree().change_scene("res://TitleScreen/Title Screen.tscn");
