extends Resource;

export(String) var name = "Enemy Template";
export(String) var class_name = "Warlock";
export(int) var armor_class = 10;
export(int) var magic_resistance = 5;
export(int) var max_health = 10;
export(int) var max_special = 10;
export(bool) var is_player = false;
export(Texture) var sprite;
export(int) var initiative_modifier = 0;
export(String) var weapon_name = "Test weapon";
export(int) var weapon_die_faces = 4;
export(int) var weapon_die_count = 1;
export(int) var magic_die_faces = 4;
export(int) var magic_die_count = 1;
# export(int) var hit_modifier = 0; # Maybe later

export(Array) var spells = Array();