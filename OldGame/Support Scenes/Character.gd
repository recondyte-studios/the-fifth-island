extends Node;
class CurrentState:
	export(int) var health = 10;
	export(int) var special = 10;
	export(int) var damage_die_modifier = 10;
	export(int) var magic_die_modifier = 10;
	export(int) var initiative = 10;
	export(bool) var is_dead = false;

export(Resource) var character_sheet = preload("res://Support Scenes/CharacterSheet.gd").new();
var current_state = CurrentState.new();
var is_raging = false;

func full_restore():
	restore_special();
	restore_health();
	
func get_name():
	return character_sheet.name;
func get_spritesheet():
	return character_sheet.sprite;

func restore_special():
	current_state.special = character_sheet.max_special;
func restore_health():
	current_state.health = character_sheet.max_health;

func init_battle():
	current_state.damage_die_modifier = 0;
	current_state.magic_die_modifier = 0;
	current_state.initiative = 0;
	
func damage(amount):
	print("Dealing damage ", amount);
	current_state.health = max(0, current_state.health - amount);
	
func heal(amount):
	if current_state.health == character_sheet.max_health:
		return false;
	current_state.health = min(character_sheet.max_health, current_state + amount);
	return true;
func get_hp():
	return current_state.health;
func get_max_hp():
	return character_sheet.max_health;
func get_sp():
	return current_state.special;
func get_max_sp():
	return character_sheet.max_special;
	
func get_is_dead():
	return current_state.is_dead;
	
func roll_initiative():
	current_state.initiative = roll_die(1,20) + character_sheet.initiative_modifier;
func get_initiative():
	return current_state.initiative;
	
func check_ac(hit):
	print("ac check");
	prints(hit, character_sheet.armor_class);
	if hit > 7:
		return true;
	return false;
func check_mr(hit):
	print("mr check");
	prints(hit, character_sheet.magic_resistance);
	if hit > 7:
		return true;
	return false;
	
func check_death():
	if get_hp() == 0:
		current_state.is_dead = true;

func roll_die(count, faces):
	var accumulator = 1;
	for i in range(count):
		accumulator += (randi() % faces - 1) + 1;
	return accumulator;
func roll_spell(spell):
	return roll_die(spell.die_count, spell.die_faces);

func use_special(amount):
	current_state.special = max(0, current_state.special - amount);
func can_use_special(amount):
	if current_state.special < amount:
		return false;
	else:
		return true;
	
func get_spell_names():
	var names = Array();
	for spell in character_sheet.spells:
		var full_spell = load(spell);
		names.append(full_spell.spell_name + String(full_spell.special_used) + "SP");
	return names;
	
func get_no_spells():
	return character_sheet.spells.size();
func get_spell_animation(spellno):
	var spell = load(character_sheet.spells[spellno]);
	return spell.animation_name;
	
func do_spell(target, spellno):
	var spell = load(character_sheet.spells[spellno]);
	print("Doing spell ", spell.spell_name);
	var can_hit = false;
	if !can_use_special(spell.special_used):
		return -2;
	# Remove the amount of special the spell uses
	prints("Special used:", spell.special_used);
	use_special(spell.special_used);
	match spell.spell_type:
		spell.SpellType.PHYSICAL:
			if is_raging:
				match spell.spell_name:
					"Beatin' Bo'Axe":
						spell.die_faces = 10;
					"Heavy Attack":
						spell.die_faces = 12;
			can_hit = check_ac(roll_die(1,20));
		spell.SpellType.MAGIC:
			can_hit = check_mr(roll_die(1,20));
		spell.SpellType.HEAL:
			can_hit = true;
		spell.SpellType.SELF:
			if spell.spell_name == "Rage":
				is_raging = true;
	if !can_hit:
		return -1;
	var spell_result = roll_spell(spell);
	if spell.spell_type == spell.SpellType.HEAL:
		heal(spell_result);
	else:
		target.damage(spell_result);
		target.check_death();
		check_death();
	return spell_result;