extends Resource;

export(String) var spell_name = "Attack";
export(int) var die_count = 1;
export(int) var die_faces = 8;
export(int) var special_used = 0;
enum SpellType {
	PHYSICAL=0, MAGIC=1, HEAL=2, SELF=3
}
export(SpellType) var spell_type = SpellType.PHYSICAL;
export(String) var animation_name = "BasicSlice";
