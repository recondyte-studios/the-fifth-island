extends Container

var select = [];
var selectedButton;
var current = 1;


func _ready():
	
	select = get_children();
	select.pop_front();
	
	
	
	pass

func _process(delta):
	
	select();
	
	pass

func select():
	
	selectedButton = select[current];
	
	selectedButton.add_color_override("font_color", Color("#b00b13"));
	
	if (Input.is_action_just_pressed('ui_up') && current != 1):
		selectedButton.add_color_override("font_color", Color("#FFFFFF"));
		current -= 1;
		print(select[current]);
		
	elif (Input.is_action_just_pressed('ui_down') && current != 3):
		selectedButton.add_color_override("font_color", Color("#FFFFFF"));
		current += 1;
		print(select[current]);
	
	if (Input.is_action_just_pressed('ui_select')):
		
		get_node("AnimationPlayer").play("fade_out");
		
		print(selectedButton.get_script());
	
	pass

func _on_AnimationPlayer_animation_finished(anim_name):
	
	selectedButton.button_pressed = true;
	selectedButton.execute();
	
	pass
