extends KinematicBody2D;

export(float) var movement_duration = 1;
const SPRITE_SIZE = 128;
var next_target = Vector2();
var is_moving = false;

func movement_done(object, key):
	is_moving = false;
	
func _process(delta):
	get_input();

func get_input():
	if is_moving:
		return;
	if Input.is_action_pressed('Right'):
		move_in_direction(1,0);
	elif Input.is_action_pressed('Left'):
		move_in_direction(-1,0);
	elif Input.is_action_pressed('Down'):
		move_in_direction(0,1);
	elif Input.is_action_pressed('Up'):
		move_in_direction(0,-1);

func move_in_direction(x, y):
	var tile_map = get_node("../TileMap");
	# Set the next place we need to go to the map coordinate at the offset given
	var next_coordinate = tile_map.world_to_map(global_position) + Vector2(x,y);
	next_target = tile_map.map_to_world(next_coordinate);
	is_moving = true;
	get_node("Tween").interpolate_property(self, "position", position, next_target, movement_duration, Tween.TRANS_LINEAR, Tween.EASE_IN , 0);
	get_node("Tween").start();
