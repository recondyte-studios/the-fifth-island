extends KinematicBody2D

const MAX_X_VELOCITY = 350;
const RUN_SPEED = 90;
const JUMP_SPEED = -400;
const FRICTION = 70;
const GRAVITY = 500;

var floor_pixel = -350;

var velocity = Vector2();
var is_jumping = false;
var has_doublejumped = false;

func get_input():
	var right = Input.is_action_pressed("right");
	var left = Input.is_action_pressed("left");
	var jump = Input.is_action_just_pressed("up");
	
	if jump and is_on_floor():
		is_jumping = true;
		has_doublejumped = false;
		velocity.y = JUMP_SPEED;
	elif jump:
		if !has_doublejumped:
			has_doublejumped = true;
			velocity.y = JUMP_SPEED;
	
	if right:
		velocity.x = min( MAX_X_VELOCITY, velocity.x + RUN_SPEED);
		turn_right();
	if left:
		turn_left();
		velocity.x = max(-MAX_X_VELOCITY, velocity.x - RUN_SPEED);
	# if right xor left
	if right != left:
		start_animation();
	else:
		stop_animation();
	
func _physics_process(delta):
	if position.y > floor_pixel:
		get_game_root().sub_score(5);
		reset_player();
		return;
	get_input();
	velocity.y += GRAVITY * delta;
	if velocity.x < 0:
		velocity.x = min(0, velocity.x + FRICTION);
	elif velocity.x > 0:
		velocity.x = max(0, velocity.x - FRICTION);
	if is_jumping and is_on_floor():
		is_jumping = false;
	velocity = move_and_slide(velocity, Vector2(0, -1));
	if is_on_floor():
		position += get_floor_velocity();
	
func reset_player():
	velocity = Vector2();
	get_game_root().get_level_node().reset_player();
	
func get_game_root():
	return get_node("..");

func get_sprite():
	return get_node("Sprite");

func turn_left():
	get_sprite().flip_h = true;
func turn_right():
	get_sprite().flip_h = false;

func get_animation_node():
	return get_node("PlayerAnimations");

func start_animation():
	if get_animation_node().current_animation != "move":
		get_animation_node().play("move");
func stop_animation():
	get_animation_node().stop(false);