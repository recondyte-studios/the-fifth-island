extends Area2D

func _on_Area2D_body_entered(body):
	if Input.is_action_pressed('secret_key'):
		PlayerData.battle_name = "Secret";
	else:
		PlayerData.battle_name = "Test";
	get_tree().change_scene("res://Battle/BattleRoot.tscn");
