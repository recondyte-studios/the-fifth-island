extends KinematicBody2D;

export(float) var movement_duration = 1.2;
const SPRITE_SIZE = 128;
var old_position = Vector2();
var next_target = Vector2();
var current_position = 0.0;
var is_moving = false;
var is_running = false;
var direction = 1; # 0 is up, 1 is down, 2 is left, 3 is right

# Used for when you walk onto an action tile
var is_next_tile_action = false;
var next_tile_name = "";

# Get our collision detecting raycaster.
onready var ray = $CollisionDetector;

# from 1 to 2
const RUN_SPEED: float = 1.5;

func _ready():
	global_position = $"../TileMap".map_to_world($"../TileMap".spawn_position);
	direction = $"../TileMap".spawn_direction;
	start_idle();
	
	
func movement_done(object, key):
	# Check if is_next_tile_action. If so, call the tileset's action tile function.
	if is_next_tile_action:
		$"../TileMap".tile_set.action_tile(next_tile_name);
		is_next_tile_action = false;
	is_moving = false;
	if get_input():
		return;
	$Animator.stop();
	start_idle();
	
		

func start_idle():
	if direction == 0:
		start_animation('UpIdle', false);
	elif direction == 1:
		start_animation('DownIdle',false);
	elif direction == 2:
		start_animation('LeftIdle',false);
	elif direction == 3:
		start_animation('RightIdle',false);

func _process(delta):
	get_input();

func get_input():
	if is_moving:
		return;
	if Input.is_action_pressed('Run'):
		is_running = true;
	else:
		is_running = false;
	if Input.is_action_pressed('Right'):
		if move_in_direction(1,0):
			start_animation('RightWalk', is_running);
			direction = 3;
			return true;
	elif Input.is_action_pressed('Left'):
		if move_in_direction(-1,0):
			start_animation('LeftWalk', is_running);
			direction = 2;
			return true;
	elif Input.is_action_pressed('Down'):
		if move_in_direction(0,1):
			start_animation('DownWalk', is_running);
			direction = 1;
			return true;
	elif Input.is_action_pressed('Up'):
		if move_in_direction(0,-1):
			start_animation('UpWalk', is_running);
			direction = 0;
			return true;

# Returns true if the movement actually occurred.
func move_in_direction(x, y):
	var tile_map = $"../TileMap";
	# Set the next place we need to go to the map coordinate at the offset given
	var next_coordinate = tile_map.world_to_map(global_position) + Vector2(x,y);
	var next_target = tile_map.map_to_world(next_coordinate);
	# Check if we can actually move to that tile
	if will_hit_tile(next_target, tile_map):
		return false;
	var move_duration = movement_duration;
	if is_running:
		move_duration *= (RUN_SPEED - 1);
	is_moving = true;
	$Tween.interpolate_property(self, "global_position", global_position, next_target, move_duration, Tween.TRANS_LINEAR, Tween.EASE_IN , 0);
	$Tween.start();
	return true;

func will_hit_tile(next_coordinate, tile_map):
	ray.cast_to = next_coordinate - global_position;
	ray.force_raycast_update();
	if ray.is_colliding():
		# Figure out if the tile is an action tile or not.
		var tile_coord = tile_map.world_to_map(next_coordinate);
		var tile_name: String = tile_map.tile_set.tile_get_name(tile_map.get_cellv(tile_coord))
		if tile_name.begins_with("action"):
			is_next_tile_action = true;
			next_tile_name = tile_name;
			return false;
		return true;
	return false;

func start_animation(animation_name: String, is_running: bool):
	if $Animator.current_animation == animation_name:
		# Animation is already playing, so we don't wanna start it again.
		return;
	if is_running:
		$Animator.play(animation_name, -1, RUN_SPEED);
	else:
		$Animator.play(animation_name);
