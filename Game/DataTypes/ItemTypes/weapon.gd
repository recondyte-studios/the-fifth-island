extends Resource
class_name Weapon

enum WeaponType { BOW, SWORD, DAGGER, WAND, STAFF };

export(String) var name: String = "Unending Tyranny, Heirloom of the Cataclysm";
export(int) var pm_hit_modifier: int = 1;
export(int) var am_hit_modifier: int = -1;
export(Resource) var modifiers: Resource = Modifiers.new();
export(int) var damage_die_faces: int = 4;
export(int) var damage_die_count: int = 2;
export(WeaponType) var type: int = WeaponType.DAGGER;
export(Texture) var icon: Texture;