extends Resource
class_name Consumable

enum ConsumableType { POTION, ELIXIR, POISON };

export(String) var name: String = "Weak Potion";
export(ConsumableType) var type: int = ConsumableType.POTION;
export(Resource) var modifiers: Resource = Modifiers.new();
export(int) var buff_id: int = 0;
export(Texture) var icon: Texture;