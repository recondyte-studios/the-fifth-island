extends Resource
class_name Accessory

export(String) var type: String = "Ring";
export(String) var name: String = "My Precious"
export(Resource) var modifiers: Resource = Modifiers.new();
export(Texture) var icon: Texture;