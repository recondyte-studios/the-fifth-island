extends Resource
class_name Buff

# Special will be hardcoded
enum BuffType { BUFF, DEBUFF, SPECIAL }

export(String) var name: String = "Auto heal";
export(BuffType) var type: int= BuffType.BUFF;
export(Resource) var modifiers: Resource = Modifiers.new();
export(int) var buff_duration: int = 0;
export(Texture) var icon: Texture;