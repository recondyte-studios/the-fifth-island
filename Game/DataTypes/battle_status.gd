extends Resource
class_name BattleStatus

# The buffs the person currently has. This needs to be cleared after a battle
export(Array, int) var current_buffs = Array();
# The amount of moves each one of these buffs has remaining
export(Array, int) var buffs_remaining_times = Array();

export(int) var current_hp: int = 0;
export(int) var current_sp: int = 0;

# We use this to figure out if that character is currently an enemy or not in the combat system
export(bool) var is_enemy: bool = false;

func get_buffs_modifiers() -> Modifiers:
	var tmp_modifiers: Modifiers = Modifiers.new();
	# Get all the modifiers of the buffs
	for buff_id in current_buffs:
		# Get the actual buff
		var buff: Buff = GlobalData.buffs[buff_id];
		# Merge its modifiers with our temporary one
		tmp_modifiers = tmp_modifiers.merge_with(buff.modifiers);
	return tmp_modifiers;