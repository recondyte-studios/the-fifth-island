extends Resource
class_name Inventory

export(int) var helmet_id: int = 0;
export(int) var body_id: int = 0;
export(int) var boots_id: int = 0;

export(int) var weapon_id: int = 0;

export(Array, int) var equipped_accessories: Array = [];

# Get the total modifiers
func get_full_modifiers():
	var tmp_modifiers = Modifiers.new();
	tmp_modifiers = tmp_modifiers.merge_with(GlobalData.armor[helmet_id].modifiers);
	tmp_modifiers = tmp_modifiers.merge_with(GlobalData.armor[body_id].modifiers);
	tmp_modifiers = tmp_modifiers.merge_with(GlobalData.armor[boots_id].modifiers);
	tmp_modifiers = tmp_modifiers.merge_with(GlobalData.weapons[weapon_id].modifiers);
	return tmp_modifiers;