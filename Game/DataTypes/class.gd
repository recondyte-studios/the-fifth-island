extends Resource
class_name Class

export(String) var name: String = "Class";
export(Texture) var icon: Texture;

export(int) var hp_percentage: int = 50;
export(int) var sp_percentage: int = 50;