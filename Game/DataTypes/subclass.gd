extends Resource
class_name Subclass

export(String) var name: String = "Test subclass";
export(Array, int) var spell_ids: Array = Array();
export(Texture) var icon: Texture;
