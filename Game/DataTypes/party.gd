extends Resource
class_name Party

export(Array, Resource) var characters: Array = [];
export(int) var party_leader: int = 0;
export(int) var gold: int = 0;

export(int) var event_counter = 0;

export(String) var current_map: String = "";
export(Vector2) var current_coordinates: Vector2

# Party inventory
export(Array, int) var armor = [];
export(Array, int) var accessories = [];
export(Array, int) var consumables = [];

func add_character(character: CharacterSheet, reset_character=true):
	if not character:
		push_error("Character is not an actual character");
	var new_char = character.duplicate(true);
	if reset_character:
		new_char.reset();
	characters.append(new_char);