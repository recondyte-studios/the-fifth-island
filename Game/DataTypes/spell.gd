extends Resource
class_name Spell

enum SpellMethod { PHYSICAL, ARCANE, LIGHT };
enum DamageType { NONE, ACID, BLUDGEONING, COLD, FIRE, FORCE, LIGHTNING, NECROTIC, PIERCING, POISON, PSYCHIC, RADIANT, SLASHING, THUNDER }; 

export(String) var name: String = "Spell test";
export(SpellMethod) var method: int = SpellMethod.PHYSICAL;
export(DamageType) var type: int = DamageType.ACID;
export(Texture) var icon: Texture;
export(bool) var applies_buff: bool = false;
export(int) var buff_id: int = 0;
export(int) var damage_die_faces: int = 4;
export(int) var damage_die_count: int = 2;
export(int) var damage_modifier: int = 0;