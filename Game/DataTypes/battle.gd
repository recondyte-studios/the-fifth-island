extends Resource
class_name Battle

export(Array, int) var enemy_list: Array;

# Will signal if the battle will be dynamically generated.
# If it is, it will pick a random set of enemies from the enemy_list
export(bool) var is_randomly_generated: bool = false;
# Only needs to be set if the is_randomly_generated is set to true. Gets the max amount of enemies generated.
export(int) var max_enemies: int = 0;

# Will signal to the battle handler that this is a special battle.
export(bool) var is_special: bool = false;
export(Resource) var special_battle_script: Resource = Modifiers.new();