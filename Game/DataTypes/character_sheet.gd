extends Resource
class_name CharacterSheet

# Used for utility
enum DamageType { NONE, ACID, BLUDGEONING, COLD, FIRE, FORCE, LIGHTNING, NECROTIC, PIERCING, POISON, PSYCHIC, RADIANT, SLASHING, THUNDER }; 

export(String) var name: String = "Test Character";

#These should all be immutable. Do any sort of modifiers in the current_modifiers property.
#The only reason these should change is for level ups or any other permanent change.
export(int) var class_id: int = 0;
export(int) var subclass_id: int = 0;
export(int) var level: int = 1;
export(int) var initiative_modifier: int = 0;
export(int) var hit_points: int = 10;
export(int) var special_points: int = 10;
export(int, 20) var physical_armor_class: int = 10;
export(int, 20) var arcane_armor_class: int = 10;

export(Resource) var equiped_items: Resource = Inventory.new();

export(Array, int) var current_spells: Array = [];

export(DamageType) var resistance_type: int = DamageType.NONE;
export(float, 0, 1, 0.01) var resistance_multiplier: float = 0.5;

# 0 Talk Sprite
# 1 Battle Sprite
# 2 Overworld Sprite
export(Array) var sprites_for_character: Array;

# This will store the character's current temporary status such as buffs, etc.
export(Resource) var battle_status: Resource;

func reset():
	battle_status = BattleStatus.new();
	battle_status.current_hp = hit_points;
	battle_status.current_sp = special_points;

func get_modifiers():
	return equiped_items.get_full_modifiers().merge_with(battle_status.get_buffs_modifiers());