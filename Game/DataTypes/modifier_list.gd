extends Resource
class_name Modifiers


# Will hold a list of modifiers that are possible in the game.
# We can use this with an `apply_modifier()` function or something so that we can easily
# apply modifiers to a character.

export(int) var arcane_ac: int = 0;
export(int) var physical_ac: int = 0;
export(int) var initiative_modifier: int = 0;
export(int) var am_hit_modifier: int = 0;
export(int) var pm_hit_modifier: int = 0;
export(int) var am_damage_modifier: int = 0;
export(int) var pm_damage_modifier: int = 0;
export(int) var lm_healing_modifier: int = 0;
export(int) var sp_modifier: int = 0;
export(int) var hp_modifier: int = 0;

# !!!!!!!!!!!!!
# DO NOT FORGET TO UPDATE THE `merge_with()` FUNCTION
# !!!!!!!!!!!!!

func merge_with(other):
	var tmp_modifier = self.duplicate(false);
	# Check to see if the other modifier even exists
	if other == null:
		# Just return the current modifier
		return tmp_modifier;
	tmp_modifier.arcane_ac += other.arcane_ac;
	tmp_modifier.physical_ac += other.physical_ac;
	tmp_modifier.initiative_modifier += other.initiative_modifier;
	tmp_modifier.am_hit_modifier += other.am_hit_modifier;
	tmp_modifier.pm_hit_modifier += other.pm_hit_modifier;
	tmp_modifier.am_damage_modifier += other.am_damage_modifier;
	tmp_modifier.pm_damage_modifier += other.pm_damage_modifier;
	tmp_modifier.lm_healing_modifier += other.lm_healing_modifier;
	tmp_modifier.sp_modifier += other.sp_modifier;
	tmp_modifier.hp_modifier += other.hp_modifier;
	return tmp_modifier;