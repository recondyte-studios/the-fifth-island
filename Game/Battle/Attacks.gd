extends Container

var currently_selected_button = 0;
var is_focused = false;
var skip_next_tick = false;

func _process(delta):
	if skip_next_tick:
		skip_next_tick = false;
		return;
	if is_focused:
		update_ui_input();
		update_ui_selection();

func set_focus():
	skip_next_tick = true;
	is_focused = true;
func unfocus():
	is_focused = false;
	
func ui_unselect_all():
	# set all the buttons to their default color:
	for child in get_children():
		set_font_color(child, "#000000");
	
func update_ui_selection():
	ui_unselect_all();
	set_font_color(get_child(currently_selected_button), "#b00b13");
	
func set_font_color(button, color):
	button.add_color_override("font_color", Color(color));

func update_ui_input():
	if Input.is_action_just_pressed('ui_up'):
		select_next_child();
	elif Input.is_action_just_pressed('ui_down'):
		select_prev_child();
	elif Input.is_action_just_pressed('ui_select'):
		do_selection();
		return;

func select_prev_child():
	currently_selected_button += 1;
	if currently_selected_button >= get_children().size():
		currently_selected_button = 0;
func select_next_child():
	currently_selected_button -= 1;
	if currently_selected_button < 0:
		currently_selected_button = get_children().size() - 1;

func do_selection():
	get_node("../Enemy Info").set_focus();
	unfocus();
	
func set_attack_name(no, attack_name):
	get_child(no).text = attack_name;


