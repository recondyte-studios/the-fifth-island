extends Node
class_name CombatSystem

signal turn_ended(current_character, is_player);
signal spell_missed(doer, doer_is_player, target, target_is_player);
signal spell_hit(doer, doer_is_player, target, target_is_player, damage, spell);
signal turn_started(current_character, is_player);

enum SpellMethod { PHYSICAL, ARCANE, LIGHT };
enum DamageType { NONE, ACID, BLUDGEONING, COLD, FIRE, FORCE, LIGHTNING, NECROTIC, PIERCING, POISON, PSYCHIC, RADIANT, SLASHING, THUNDER }; 

var enemies: Array = Array();
var enemy_initiatives: Array = Array();
var party_initiatives: Array = Array();
var turn_count: int = 0;

var current_character: int = 0;
var current_initiative: int = 0;
var current_is_player: bool = false;

var rng: RandomNumberGenerator = RandomNumberGenerator.new();

func _init():
	rng.randomize();

func init_battle(battle_id: int) -> void:
	enemy_initiatives.clear();
	party_initiatives.clear();
	turn_count = 0;
	current_character = 0;
	current_is_player = false;
	enemies.clear();
	# Initialize the battle data we need.
	var battle: Battle = GlobalData.battles[battle_id];
	# Figure out if it's a random set or not
	if battle.is_randomly_generated:
		# Figure out how many enemies the battle will have
		var num_enemies = rng.randi_range(1, battle.max_enemies);
		for i in range(num_enemies):
			var enemy_id = rng.randi_range(0, battle.enemy_list.size() - 1);
			print(GlobalData.characters[enemy_id].name);
			var enemy: CharacterSheet = GlobalData.characters[enemy_id] as CharacterSheet;
			if not enemy:
				print("Incorrect enemy");
				push_error("Incorrect enemy");
			var new_enemy = enemy.duplicate(true);
			new_enemy.reset();
			enemies.append(new_enemy);
	else:
		for enemy_id in battle.enemy_list:
			print(GlobalData.characters[enemy_id].name);
			var enemy: CharacterSheet = GlobalData.characters[enemy_id] as CharacterSheet;
			if not enemy:
				print("Incorrect enemy");
				push_error("Incorrect enemy");
			var new_enemy = enemy.duplicate(true);
			new_enemy.reset();
			enemies.append(new_enemy);
	# If it's a special battle, run its own setup script
	if battle.is_special:
		battle.special_battle_script.setup();
	next_turn();

func next_turn():
	# Figure out who goes next. On the first move, we figure out which side has the highest total initiative.
	if turn_count == 0:
		# Go through the party list and sum up all the initiatives
		var party_accumulator: int = 0;
		for i in range(party_initiatives.size()):
			var init: int = GlobalData.party.characters[i].get_modifiers().initiative_modifier;
			init += party_initiatives[i];
			party_accumulator += init;
		var enemy_accumulator: int = 0;
		for i in range(enemy_initiatives.size()):
			var init: int = enemies[i].get_modifiers().initiative_modifier;
			init += enemy_initiatives[i];
			enemy_accumulator += init;
		current_is_player = party_accumulator >= enemy_accumulator;
		# Set the current initiative to 9999 to make sure that we get the next player with higheset initiative
		current_initiative = 9999;
		
	# Figure out if we are checking the enemy or the party first
	if current_is_player:
		party_next_turn();
	else:
		enemy_next_turn();

func enemy_next_turn():
	# Get the next lowest initiative we have
	var highest_enemy: int = 0;
	var highest_enemy_initiative: int = -9999;
	for enemy_id in range(enemy_initiatives.size()):
		# If the current character is this character and we aren't trying to get the best enemy for the first time, ignore this one
		if enemy_id == current_character && current_initiative != 9999:
			return;
		# Get the initiative
		var init: int = enemies[enemy_id].get_modifiers().initiative_modifier;
		init += enemy_initiatives[enemy_id];
		# Make sure its initiative is less than the current
		if init <= current_initiative:
			if highest_enemy_initiative < init:
				highest_enemy_initiative = init;
				highest_enemy = enemy_id;
	# We didn't find any enemies, so we need to check the party instead
	if highest_enemy_initiative == -9999:
		party_next_turn();
		return;
	# We found an enemy, so we set that enemy to have the next turn
	current_character = highest_enemy;
	current_is_player = false;

func party_next_turn():
	# Get the next lowest initiative we have
	var highest_member: int = 0;
	var highest_member_initiative: int = -9999;
	for member_id in range(party_initiatives.size()):
		# If the current character is this character and we aren't trying to get the best member for the first time,
		# ignore this one
		if member_id == current_character && current_initiative != 9999:
			return;
		# Get the initiative
		var init: int = GlobalData.party.characters[member_id].get_modifiers().initiative_modifier;
		init += party_initiatives[member_id];
		# Make sure its initiative is less than the current
		if init <= current_initiative:
			if highest_member_initiative < init:
				highest_member_initiative = init;
				highest_member = member_id;
	if highest_member_initiative == -9999:
		enemy_next_turn();
		return;
	current_character = highest_member;
	current_is_player = true;

func end_turn():
	emit_signal("turn_ended", current_character, current_is_player);
	return;

func roll_die(count: int, face: int) -> int:
	var accumulator: int = 0;
	for i in range(count):
		accumulator += rng.randi_range(1, face + 1);
	return accumulator;

# Rolls initiative for all characters
func roll_initiative_all():
	# Start by rolling the initiative of each enemy
	for i in range(enemies.size()):
		# This will roll without any modifiers. This way any initiative modifiers that may happen during battle
		# will be able to update seamlessly
		enemy_initiatives.append(roll_die(1, 20));
	# Then the initiative for all the party members
	for i in range(GlobalData.party.characters.size()):
		party_initiatives.append(roll_die(1, 20));

func do_spell(target_id: int, spell_no: int, target_is_player: bool):
	# We do so many checks, this resource is practically guarenteed to be a CharacterSheet.
	var target;
	if target_is_player:
		target = GlobalData.party.characters[target_id];
	else:
		target = enemies[target_id];
	var doer;
	if current_is_player:
		doer = GlobalData.party.characters[current_character];
	else:
		doer = enemies[current_character];
	# We get the spell of the character passed
	var spell: Spell = GlobalData.spells[doer.current_spells[spell_no]];
	# We get the AC that need of the character we are trying to hit
	var ac: int = 0;
	var hit_modifier: int = 0;
	if spell.method == SpellMethod.PHYSICAL:
		ac = target.physical_armor_class;
		hit_modifier = doer.get_modifiers().pm_hit_modifier;
	elif spell.method == SpellMethod.ARCANE:
		ac = target.arcane_armor_class;
		hit_modifier = doer.get_modifiers().am_hit_modifier;
	# We make it an automatic hit if it's something we don't know
	else:
		# We set the AC to something we'd never be able to touch without it being a bug
		ac = -9999;
	# Now we roll to see if it hits
	if roll_die(1, 20) + hit_modifier > ac:
		# The spell hit. We'll put this into another function just for separation's purposes
		emit_signal("spell_hit", current_character, current_is_player, target_id, target_is_player, attack(doer, target, spell), spell);
	else:
		emit_signal("spell_missed", current_character, current_is_player, target_id, target_is_player);

func attack(doer, target, spell: Spell) -> int:
	# We need to calculate exactly the amount of damage (or healing if an LM spell) it does
	var spell_damage: int = roll_die(spell.damage_die_count, spell.damage_die_faces) + spell.damage_modifier;
	# And now we need to add in any other damage modifiers we have
	if spell.method == SpellMethod.PHYSICAL:
		spell_damage += doer.get_modifiers().pm_damage_modifier;
	elif spell.method == SpellMethod.ARCANE:
		spell_damage += doer.get_modifiers().am_damage_modifier;
	# Light method
	else:
		spell_damage += doer.get_modifiers().lm_healing_modifier;
		# Make spell_damage negative to heal the target
		spell_damage = -spell_damage;
	# And now we check if the damage resistance is the same as the spell's damage type
	if spell.type == target.resistance_type && target.resistance_type != DamageType.NONE:
		spell_damage = spell_damage * target.resistance_multiplier;
		
	# And we do the damage
	target.battle_status.current_hp -= spell_damage;
	if target.battle_status.current_hp < 0:
		target.battle_status.current_hp = 0;
	elif target.battle_status.current_hp > target.hit_points:
		target.battle_status.current_hp = target.hit_points;
	
	# And now that the damage is done, we yeet outta here
	return spell_damage
	
	
	