extends TileMap

enum Direction { UP, DOWN, LEFT, RIGHT };

export(Vector2) var spawn_position: Vector2 = Vector2(0,0);
export(Direction) var spawn_direction: int = Direction.DOWN;
