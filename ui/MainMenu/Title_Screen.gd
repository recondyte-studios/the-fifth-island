extends Control

var scene_path_to_load

func _ready():
	#$Menu/CenterRow/Buttons/StartButton.grab_focus()
	for button in $Menu/Container/Buttons.get_children():
		button.connect("pressed", self, "_on_Button_pressed", [button.scene_to_load])

func _on_Button_pressed(scene_to_load):
	scene_path_to_load = scene_to_load
	$AnimFade.show()
	$AnimFade.fade_in()
	if scene_path_to_load == null:
		get_tree().quit()

func _on_AnimFade_fade_finished():
	get_tree().change_scene(scene_path_to_load)

func _notification(what):
    if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST:
        get_tree().quit()